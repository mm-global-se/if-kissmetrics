mmcore.IntegrationFactory.register('Kissmetrics', {
    defaults: {},

    validate: function (data) {
        if(!data.campaign)
            return 'No campaign.';

        return true;
    },

    check: function (data) {
        return window.KM_KEY && window.KM.i();
    },

    exec: function (data) {
        window._kmq = window._kmq || [],
            mode = data.isProduction ? 'MM_Prod_' : 'MM_Sand_',
            campaignInfo = mode + data.campaignInfo,
            callback = data.callback || function () {};

        _kmq.push(['record', 'MVT', {'mvt': campaignInfo}, callback]);

        return true;
    }
});