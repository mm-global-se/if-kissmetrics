# Kissmetrics

---

[TOC]

## Overview

This integration is used for extracting campaign experience data from the Maxymiser platform and sending it to [Kissmerics](https://kissmetrics.com/).

## How we send the data

We add a new "record" with campaign experience information to the `window._kmq` array.

```javascript

_kmq.push(['record', 'MVT', {'mvt': campaignInfo}]);

```

It creates an image request with campaign data.

## Data Format

The data sent to Kissmetrics will be in the following format:

`<mode>_<campaign name>=<element1>:<variant>|<element2>:<variant>`

Please find additional information on the format of the sent data [here](https://bitbucket.org/mm-global-se/integration-factory#markdown-header-introduction
)

## Prerequisite

The following information needs to be provided by the client: 

+ Campaign Name

## Download

* [kissmetrics-register.js](https://bitbucket.org/mm-global-se/if-kissmetrics/src/master/src/kissmetrics-register.js)

* [kissmetrics-initialize.js](https://bitbucket.org/mm-global-se/if-kissmetrics/src/master/src/kissmetrics-initialize.js)

## Deployment instructions

### Content Campaign

+ Ensure that you have the Integration Factory plugin deployed on site level([find out more](https://bitbucket.org/mm-global-se/integration-factory)). Map this script to the whole site with an _output order: -10_ 

+ Create another site script and add the [kissmetrics-register.js](https://bitbucket.org/mm-global-se/if-kissmetrics/src/master/src/kissmetrics-register.js) script.  Map this script to the whole site with an _output order: -5_

    __NOTE!__ Please check whether the integration already exists on site level. Do not duplicate the Integration Factory plugin and the Google Analytics Register scripts!

+ Create a campaign script and add the [kissmetrics-initialize.js](https://bitbucket.org/mm-global-se/if-kissmetrics/src/master/src/kissmetrics-initialize.js) script. Customise the code by changing the campaign name, account ID (only if needed), and slot number accordingly and add to campaign. Map this script to the page where the variants are generated on, with an _output order: 0_

### Redirect Campaign

For redirect campaigns, the campaign script needs to be mapped to both the generation and redirected page. To achieve this follow the steps bellow:

+ Go through the instructions outlined in [Content Campaign](https://bitbucket.org/mm-global-se/if-ga#markdown-header-content-campaign)

+ Make sure that in the initialize campaign script, redirect is set to true.

+ Create and map an additional page where the redirect variant would land on. 

+ Map the integration initialize campaign script to this page as well.

## QA

+ Open Chrome DevTools

+ Go to 'Network' tab

+ Filter by `kissmetrics`

+ Under 'Header' tab find `mvt` parameter with campaign inforamtion

![qa](assets/kiss.png)